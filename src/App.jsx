import './App.css'
import { Banner } from './assets/components/banner/Banner';
import { CardsLayout } from './assets/components/cards-layout/CardsLayout';

function App() {

  return (
    <>
    <Banner/>
    <CardsLayout/>
    </>

  );
};

export default App
