import React from 'react'
import "./banner.css"

export const Banner = () => {
	return (
		<div className='banner-container py-2'>
			<h1>Aprendiendo React en PWC</h1>
		</div>
	)
}
