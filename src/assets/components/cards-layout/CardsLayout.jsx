import React, { useState } from 'react'
import { Card } from '../card/Card'

import "./cards-layout.css"

export const CardsLayout = () => {
	const [index, setIndex] = useState(null)
	
	const handleClick = (clickedIndex) =>{
		setIndex((prevIndex) => (prevIndex === clickedIndex ? null : clickedIndex));
	}

	return (
		<div className='layout container mx-auto'>
			<Card 
				title={"Hola"}
				image={"https://images.unsplash.com/photo-1675573789887-5c0756764df4?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=681&q=80"}
				description={"ofjafdkasjdaksjdksjd"}
				selected={index === 0}
				onSelected={() => handleClick(0)}
			/>
			<Card 
				title={"Hola"}
				image={"https://images.unsplash.com/photo-1675573789887-5c0756764df4?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=681&q=80"}
				description={"ofjafdkasjdaksjdksjd"}
				selected={index === 1}
				onSelected={() => handleClick(1)}
			/>
			<Card 
				title={"Hola"}
				image={"https://images.unsplash.com/photo-1675573789887-5c0756764df4?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=681&q=80"}
				description={"ofjafdkasjdaksjdksjd"}
				selected={index === 2}
				onSelected={() => handleClick(2)}
			/>


		</div>
	)
}
