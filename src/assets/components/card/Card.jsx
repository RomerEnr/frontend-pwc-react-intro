import './card.css';

export const Card = ({ image, title, description, selected, onSelected }) => {


  const cardClass = selected ? 'card selected' : 'card';

  return (
    <div className={cardClass} onClick={onSelected}>
      <img className="card-image" src={image} alt={title} />
      <h2 className="card-title">{title}</h2>
      <p className="card-description">{description}</p>
    </div>
  );
};